#!/bin/bash
#
# particon del disco

PS='Seleccione el modo de partición: '
option1='boot + 1 disco'
option2="boot + 3 discos"
options=("$option1" "$option2" "Quit")

read -r -p " disco que desea particionar: " DISK
read -r -p " esta seguro? se borraran todos los datos de $DISK [y/N] " RESPONSE1

if [[ ! "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
	exit
fi

select opt in "${options[@]}"
do
    case $opt in
        "$option1")
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | fdisk $DISK
	g      # crea particion GPT
	n      # add new partition
	1      # partition number
	       # default - first sector 
	+550MiB# partition size
	n      # add new partition
	2      # partition number
	       # default - first sector 
	       # default - last sector 
	t      # partition type
	1      # partition number
	1      # EFI System
	t      # partition type
	2      # partition number
	20     # linux filesystem
	w      # write partition table and exit
FDISK_CMDS
			break
            ;;
        "$option2")
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | fdisk $DISK
	g      # crea particion GPT
	n      # add new partition
	1      # partition number
	       # default - first sector 
	+550MiB# partition size
	n      # add new partition
	2      # partition number
	       # default - first sector 
	+23GiB # default - last sector 
	n      # add new partition
	3      # partition number
	       # default - first sector 
	+550MiB# default - last sector 
	n      # add new partition
	4      # partition number
	       # default - first sector 
	       # default - last sector 
	t      # partition type
	1      # partition number
	1      # EFI System
	t      # partition type
	2      # partition number
	24     # Linux root x86-64
	t      # partition type
	3      # partition number
	19     # linux swap
	t      # partition type
	4      # partition number
	28     # linux home
	w      # write partition table and exit
FDISK_CMDS
			break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
