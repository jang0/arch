USER = "jango"
PWD = $(shell pwd)
all:
	echo "push and pull"
push:
	rm -rf config
	mkdir config
	yes | cp -ir /home/$(USER)/.config/i3	$(PWD)/config	
	yes | cp -ir /home/$(USER)/.config/ranger	$(PWD)/config	
	yes | cp -ir /home/$(USER)/.config/nvim		$(PWD)/config	
	yes | cp -ir /home/$(USER)/.config/zathura	$(PWD)/config	
	yes | cp -ir /home/$(USER)/.local/share/OpenSCAD	$(PWD)/config	
	yes | cp -i  /home/$(USER)/.bashrc	$(PWD)/config	
	yes | cp -i  /home/$(USER)/.bash_profile	$(PWD)/config	
	yes | cp -i  /home/$(USER)/.Xresources	$(PWD)/config	
	yes | cp -i  /home/$(USER)/.xinitrc	$(PWD)/config	
	yes | cp -i  /etc/systemd/system/wakelock.service	$(PWD)/config	
pull: 
	yes | cp -ir $(PWD)/config/i3	/home/$(USER)/.config        
	yes | cp -ir $(PWD)/config/ranger	/home/$(USER)/.config
	yes | cp -ir $(PWD)/config/nvim	/home/$(USER)/.config
	yes | cp -ir $(PWD)/config/zathura	/home/$(USER)/.config
	yes | mkdir -p	/home/$(USER)/.local/share
	yes | cp -ir $(PWD)/config/OpenSCAD	/home/$(USER)/.local/share
	yes | cp -i  $(PWD)/config/.bashrc	/home/$(USER)/
	yes | cp -i  $(PWD)/config/.bash_profile	/home/$(USER)/
	yes | cp -i  $(PWD)/config/.Xresources	/home/$(USER)/
	yes | sudo cp -i  $(PWD)/config/.xinitrc	/home/$(USER)/
	yes | sudo cp -i  $(PWD)/config/wakelock.service	/etc/systemd/system/        
	sudo systemctl enable wakelock.service
