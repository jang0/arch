#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# librerias de openscad
OPENSCADPATH=/home/jango/Dropbox/openscad/libraries
