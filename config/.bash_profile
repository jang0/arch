#
# ~/.bash_profile
#
#cargar bashrc
[[ -f ~/.bashrc ]] && . ~/.bashrc

# iniciar startx automaticamente
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
