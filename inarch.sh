#!/bin/bash
#

# Disco de destino
read -r -p "disco en el que desea instalar archlinux: " DISK

# Encriptacion
read -r -p "desea encriptar el disco [y/N] " RESPONSE1

# Flags
USERNAME="jango"
gitSource="https://gitlab.com/jang0/arch"
swapSize="20GB"
BOOTPART=$DISK'1'
PACMAN='pacman -S --noconfirm'
if [[ "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
	lvmpool='l'
	ROOTPART=/dev/mapper/$lvmpool'-root'
	SWAPPART=/dev/mapper/$lvmpool'-swap'
	HOMEPART=/dev/mapper/$lvmpool'-home'
else
	ROOTPART=$DISK'2'
	SWAPPART=$DISK'3'
	HOMEPART=$DISK'4'
fi

# comprobación del modo UEFI
if [ ! -e /sys/firmware/efi/efivars ]
then
    echo 'el sistema no se inició en modo UEFI, seleccionar esta opción al bootear la USB'
    exit
fi



if [[ "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
	# load the kernel module explicitly
	modprobe dm_crypt
	
	# encriptacion
	cryptsetup -y -v luksFormat $DISK'2' || { echo >&2 "error en la encriptacion"; exit 1; }
	
	# verificacion
	cryptsetup luksDump $DISK'2'
	
	# apertura de la particion encriptada
	cryptsetup open $DISK'2' crypt
	
	# LVM configuracion
	lvm pvcreate /dev/mapper/crypt
	lvm vgcreate $lvmpool /dev/mapper/crypt
	lvm lvcreate -l 50%FREE -n root $lvmpool
	lvm lvcreate -L $swapSize -n swap $lvmpool
	lvm lvcreate -l 100%FREE -n home $lvmpool
	lvm lvs
fi


# formatear
mkfs.vfat $BOOTPART
mkfs.ext4 $ROOTPART
mkfs.ext4 $HOMEPART
mkswap $SWAPPART
swapon $SWAPPART

# montaje de los discos
mount $ROOTPART /mnt
mkdir -p /mnt/home /mnt/boot 
mount $BOOTPART /mnt/boot
mount $HOMEPART /mnt/home

# asignar nombre de particiones a variables
if [[ "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
	UUID="$( blkid | grep $DISK'2' | awk -F' UUID=\"' '{print $2}' | awk -F"\"" '{print $1}' )"
else
	UUID="$( blkid | grep $ROOTPART | awk -F' PARTUUID=\"' '{print $2}' | awk -F'\"' '{print $1}' )"
fi

#---------------instalacion archlinux-------------------

# instalación de los paquetes base
pacstrap /mnt 

# generar fstab
genfstab -U -p /mnt > /mnt/etc/fstab

# cambio de sesion
arch-chroot /mnt << EOF

# bootloader
bootctl install

# programas necesarios para realizar la configuracion posterior a la instalacion
$PACMAN git dialog wpa_supplicant neovim

# configuracion mkinitcpio
if [[ "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
sed -i 's/^HOOKS=.*/HOOKS=(base udev autodetect modconf block filesystems keymap encrypt lvm2 keyboard fsck)/g' /etc/mkinitcpio.conf
fi

# configuracion loader
echo "default inicio" > /boot/loader/loader.conf
echo 'linux /vmlinuz-linux' > /boot/loader/entries/inicio.conf
echo 'initrd /initramfs-linux.img' >> /boot/loader/entries/inicio.conf
if [[ "$RESPONSE1" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
echo "options rw cryptdevice=UUID=$UUID:crypt root=$ROOTPART" >> /boot/loader/entries/inicio.conf
else
echo "options rw root=PARTUUID=$UUID" >> /boot/loader/entries/inicio.conf
fi

mkinitcpio -p linux

# nuevo usuario
echo 'contraseña para root:'
passwd
useradd -m -g users -G wheel,storage,audio -s /bin/bash $USERNAME
echo 'contraseña para $USERNAME:'
passwd $USERNAME
pacman -S --noconfirm sudo
sed -i '/%wheel ALL=(ALL) ALL/s/^#//' /etc/sudoers

#archivos siguiente instalacion
git clone $gitSource /home/$USERNAME/arch
chown -R $USERNAME:users /home/$USERNAME/arch

#exit

EOF

echo '-------------- INSTALACION COMPLETA --------------'
