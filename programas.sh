#!/bin/bash
# Store menu options selected by the user
INPUT=/tmp/menu.sh.$$

# Storage file for displaying cal and date command output
OUTPUT=/tmp/output.sh.$$

# trap and delete temp files
trap "rm $OUTPUT; rm $INPUT; exit" SIGHUP SIGINT SIGTERM

# funciones
pause(){
  read -p "Presione la tecla [Enter] para continuar..." fackEnterKey
}

PACMAN="sudo pacman -S --noconfirm --needed"
yay="yay -S --noconfirm --needed"
PIP="pip install --user"

# teclado
# localectl list-x11-keymap-models\n\
# localectl list-x11-keymap-layouts\n\
# localectl list-x11-keymap-variants [layout]\n\
# localectl list-x11-keymap-options"
KLAYOUT='us'
KMODEL='kinesis'
KVARIANT=''

COUNTRY='US'
REGION='America'
CITY='Bogota'
LANGUAGE='en_US.UTF-8'
LOCALE='en_US.UTF-8 UTF-8'
MYHOSTNAME='jpc'
USERNAME='jango'
WIFI='on'

#------------------------------------------------
# Propósito: crear usuarios, asignar contraseñas, permisos, reloj y lenguaje
#
titulo_a=('configuración basica')
function menu_a(){

# lista de selección
funcheck=(dialog --separate-output 
--title "$titulo_1"
--checklist "" 0 0 0
 1 "Teclado" on
 2 "Asignar contraseña para root" off 
 3 "Crear usuario $USERNAME" off
 4 "Asignar contraseña para $USERNAME" off
 5 "Permisos de SUDO" off
 6 "Lenguaje" on
 7 "HostName" on
 8 "Ubicación zona horaria" on
 9 "Acceso a usuario $USERNAME" off 
 )

selecciones=$("${funcheck[@]}" 2>&1 >/dev/tty)

#se ejecuta un comando en función de las selecciones realizadas
for seleccion in $selecciones
do
 case $seleccion in
 1)
	#Teclado en consola
	localectl set-keymap --no-convert $KLAYOUT
 ;;
 2)
	echo 'contraseña para root:'
	passwd
 ;;
 3)
	useradd -m -g users -G wheel,storage,audio -s /bin/bash $USERNAME
 ;;
 4)
	echo 'contraseña para $USERNAME:'
	passwd $USERNAME
 ;;
 5)
	pacman -S --noconfirm sudo
	sed -i '/%wheel ALL=(ALL) ALL/s/^#//' /etc/sudoers
 ;;
 6)
 	grep -q ^$LOCALE* /etc/locale.gen || sudo echo $LOCALE >> /etc/locale.gen
	localectl set-locale LANG=$LANGUAGE
	locale-gen
 ;;
 7)
	hostnamectl set-hostname $MYHOSTNAME
	touch /etc/hosts
	echo "127.0.0.1	localhost" >  /etc/hosts
	echo "::1		localhost" >> /etc/hosts
	echo "127.0.1.1	$MYHOSTNAME.localdomain	$MYHOSTNAME" >> /etc/hosts
 ;;
 8)
	ln -sf /usr/share/zoneinfo/$REGION/$CITY /etc/localtime
	hwclock --systohc
	timedatectl set-local-rtc 0
	pacman -S --noconfirm ntp
	ntpd -qg
	systemctl enable ntpd.service
 ;;
 9)
 	su - $USERNAME
 ;;

 esac
done
pause
}


#------------------------------------------------
# Propósito: configurar gestores de conexión
#
titulo_c=('Conexión')
function menu_c(){

# lista de selección
funcheck=(dialog --separate-output 
--title "$titulo_4"
--checklist "" 0 0 0
 1 "gestor AUR, python-pip" on 
 2 "librerias pacman x32 bits" on
 3 "espejos con Reflector" on 
 4 "WIFI para portatil" $WIFI
 )

selecciones=$("${funcheck[@]}" 2>&1 >/dev/tty)

#se ejecuta un comando en función de las selecciones realizadas
for seleccion in $selecciones
do
 case $seleccion in
 1)
 	$PACMAN base-devel python-pip
	git clone https://aur.archlinux.org/yay.git
	cd yay && yes | makepkg -si
 ;;
 2)
  sudo sed -i '/^#\[multilib]/{N;s/\n#/\n/}' /etc/pacman.conf
  sudo sed -i 's/#\[multilib]/\[multilib]/g' /etc/pacman.conf
	sudo pacman -Sy --noconfirm
 ;;
 3)
	$PACMAN reflector
	sudo reflector --verbose -l 25 -p http --sort rate --save /etc/pacman.d/mirrorlist
	sudo pacman -Syy
 ;;
 4)
 	$yay wicd-patched
 ;;
 esac
done
pause
}



#------------------------------------------------
# Propósito: Configurar interfaz de usuario
#
titulo_d=('Interfaz de usuario')
function menu_d(){

# lista de selección
funcheck=(dialog --separate-output 
--title "$titulo_5"
--checklist "" 0 0 0
 1 "Controlador de video" on
 2 "Controlador de sonido" on
 3 "Xorg: gestor de ventanas" on
 4 "i3: complemento Xorg" on
 5 "Terminal URXVT" on
 6 "Fuentes" on
 7 "Aplicacion archivos" on
 8 "Aplicacion ventanas" on
 9 "Aplicacion imagenes" on
10 "Comandos especiales" on
11 "Medios externos: USB, ISO, ZIP" on
 )

selecciones=$("${funcheck[@]}" 2>&1 >/dev/tty)

#se ejecuta un comando en función de las selecciones realizadas
for seleccion in $selecciones
do
 case $seleccion in
 1)
	if lspci | grep VGA | grep -iq 'virtualbox'
	then 
    		printf '2\ny' | sudo pacman -S virtualbox-guest-utils
	elif lspci | grep VGA | grep -iq 'amd\|ati'
	then
    		$PACMAN xf86-video-amdgpu
	elif lspci | grep VGA | grep -iq 'intel'
	then
    		$PACMAN xf86-video-intel
	elif lspci | grep VGA | grep -iq 'nvidia'
	then
    		$PACMAN xf86-video-nouveau
	elif lspci | grep VGA | grep -iq 'via'
	then
    		$PACMAN xf86-video-openchrome
	else 
		echo 'error al reconocer la tarjeta de video'
	fi 
	$PACMAN mesa
 ;;
 2)
	$PACMAN alsa-utils
 ;;
 3)
	$PACMAN xorg-server xorg-xinit
	sudo chmod u+s /usr/bin/Xorg
	#Teclado en xorg
	sudo localectl set-x11-keymap --no-convert $KLAYOUT "$KMODEL" "$KVARIANT" "$OPTION"
	setxkbmap $KLAYOUT -variant $KVARIANT
 ;;
 4)
	$PACMAN i3
 ;;
 5)
	$PACMAN rxvt-unicode urxvt-perls
	$yay urxvt-resize-font-git xcwd-git
 ;;
 6)
	$PACMAN ttf-font-awesome ttf-hack ttf-dejavu
 ;;
 7)
	$PACMAN rofi ranger
 ;;
 8)
	$PACMAN lxappearance arc-gtk-theme unclutter arandr
 ;;
 9)
	$PACMAN feh w3m
 ;;
 10)
	$PACMAN playerctl xautolock xorg scrot 
 ;;
 11)
	$PACMAN ntfs-3g fuseiso unzip
 ;;
 esac
done
pause
}


#------------------------------------------------
# Propósito:
#
titulo_e=('programas')
function menu_e(){

# lista de selección
funcheck=(dialog --separate-output 
--title "$titulo_6"
--checklist "" 0 0 0
 1  "Internet" on
 2  "torrent" on
 3  "Dropbox" on
 4  "PDF" on
 5  "Lectura otros idiomas" on 
 6  "Música" on
 7  "CAD" on
 8  "hack" on
 9  "Programación" on
 10 "t" on
 11 "Electrónica" on
 12 "Elementos Finitos" on
 13 "Latex, pandoc" off
 14 "emulador windows: flatpak wine" off
 15 "osp-tracker" off
 16 "c" on
 17 "i" on
 )

selecciones=$("${funcheck[@]}" 2>&1 >/dev/tty)

#se ejecuta un comando en función de las selecciones realizadas
for seleccion in $selecciones
do
 case $seleccion in
 1)
	$PACMAN qutebrowser firefox 
	$yay tor-browser
 ;;
 2)
	$PACMAN transmission-cli
 ;;
 3)
  gpg --recv-keys FC918B335044912E
	$yay dropbox 
 ;;
 4)
	$PACMAN zathura zathura-cb zathura-djvu zathura-pdf-poppler zathura-ps 
 ;;
 5)
	$PACMAN wqy-zenhei fcitx fcitx-im fcitx-configtool fcitx-sunpinyin
 ;;
 6)
	$PACMAN vlc mpd mpc ncmpcpp
	$yay spotify
 ;;
 7)
 	$PACMAN blender librecad
	$yay openscad-git openscad-scad-utils-git openscad-list-compr-demos-git
  $PIP solidpython
 ;;
 8)
  $yay airgeddon-git
 ;;
 9)
	$PACMAN make gcc-libs gcc-fortran vtk gnuplot
  $yay gmsh
	$PIP matplotlib
 ;;
 10)
   echo papas
 ;;
 11)
	$PACMAN kicad kicad-library kicad-library-3d
 ;;
 12) 
  $PACMAN openmpi
  $yay petsc deal-ii
 ;;
 13)
	$PACMAN pandoc pandoc-citeproc texlive-most biber 
	$PIP pandoc-fignos
 ;;
 14)
	$PACMAN flatpak
	flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
	flatpak remote-add --user --if-not-exists --no-gpg-verify winepak https://dl.winepak.org/repo/winepak.flatpakrepo
 ;;
 15)
	$PACMAN xdg-user-dirs
	xdg-user-dirs-update
	$yay osp-tracker
 ;;
 16)
	echo papas
 ;;
 17)
	echo papas
 ;;
 esac
done
pause
}



# ciclo infinito para ejecutar el programa
while true
do
#
# Menú principal
#
menu=(dialog --clear 
--cancel-label EXIT
--backtitle "Linux Shell Script Tutorial" 
--title "[ MENÚ PRINCIPAL ]" 
--menu "seleccione opción" 0 0 0 
 1 "$titulo_a"
 2 "$titulo_c" 
 3 "$titulo_d" 
 4 "$titulo_e" 
)

"${menu[@]}" 2>"${INPUT}"
menuitem=$(<"${INPUT}")

# configurar EXIT
if [ -z "$menuitem" ]
then
	clear
	break
fi

# asignación submenús a decisiones 
case $menuitem in 
 1) menu_a;;
 2) menu_c;;
 3) menu_d;;
 4) menu_e;;
esac

done

# borrar archivos temporales
[ -f $OUTPUT ] && rm $OUTPUT
[ -f $INPUT ] && rm $INPUT

